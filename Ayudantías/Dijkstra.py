# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 08:10:14 2020

@author: dario
"""
import math
class vertice():
    def __init__(self, identificador, valor=0):
        self.set_identificador(identificador)
        self.set_valor(valor)
        
    def set_identificador(self, identificador):
        self.__identificador = identificador
        
    def get_identificador(self):
        return self.__identificador
    
    def set_valor(self, valor):
        self.__valor = valor
        
    def get_valor(self):
        return self.__valor
    
    def __str__(self):
        return "Vertice:"+str(self.get_identificador())+" Valor:"+str(self.get_valor())
    
    def __repr__(self):
        return self.__str__()



class arista():
    def __init__(self,id_vertice_a, id_vertice_b, peso=0):
        self.set_id_vertice_a(id_vertice_a)
        self.set_id_vertice_b(id_vertice_b)
        self.set_peso(peso)
        
    def set_id_vertice_a(self, id_vertice_a):
        self.__id_vertice_a = id_vertice_a
        
    def get_id_vertice_a(self):
        return self.__id_vertice_a
    
    def set_id_vertice_b(self, id_vertice_b):
        self.__id_vertice_b = id_vertice_b
        
    def get_id_vertice_b(self):
        return self.__id_vertice_b
    
    def set_peso(self, peso):
        self.__peso = peso
    
    def get_peso(self):
        return self.__peso 
    
    def __str__(self):
        return "Arista:"+str(self.get_id_vertice_a())+"-"+str(self.get_id_vertice_b())+" Peso:"+str(self.get_peso())
    
    def __repr__(self):
        return self.__str__()



class grafica():
    
    def __init__(self, vertices, aristas):
        self.set_vertices(vertices)
        self.set_aristas(aristas)
        
    def set_vertices(self, vertices):
        self.__vertices = vertices
        
    def get_vertices(self):
        return self.__vertices
    
    def set_aristas(self, aristas):
        self.__aristas = aristas
        
    def get_aristas(self):
        return self.__aristas
    
    def get_vecinosDe(self, id_vertice):
        listaVecinos = []
        aristasgraf = self.get_aristas()
        for arista in aristasgraf:
            if id_vertice == arista.get_id_vertice_a():
                listaVecinos.append(arista.get_id_vertice_b())
            if id_vertice == arista.get_id_vertice_b():
                listaVecinos.append(arista.get_id_vertice_a())
                
        return listaVecinos
    
    def get_pesoArista(self, id_vertice_a, id_vertice_b):
        aristasgraf = self.get_aristas()
        for arista in aristasgraf: 
            if id_vertice_a == arista.get_id_vertice_a() and id_vertice_b == arista.get_id_vertice_b():
                return arista.get_peso()
            if id_vertice_a == arista.get_id_vertice_b() and id_vertice_b == arista.get_id_vertice_a():
                return arista.get_peso()
        return None
    
    def get_valorVertice(self, id_vertice):
        verticesgraf = self.get_vertices()
        for vertice in verticesgraf:
            if id_vertice == vertice.get_identificador():
                return vertice.get_valor()
        return None
        
    def set_valorVertice(self, id_vertice, valor_nuevo):
        verticesgraf = self.get_vertices()
        for vertice in verticesgraf:
            if id_vertice == vertice.get_identificador():
                vertice.set_valor(valor_nuevo)
    
    def __str__(self):
        cadena = ""
        for vertice in self.get_vertices():
            cadena += str(vertice)+"\n"
        for arista in self.get_aristas():
            cadena += str(arista)+"\n"

        return cadena

    def __repr__(self):
        return self.__str__()


class dijkstra():     
    
    def __init__(self, grafica, id_vertice_inicial, id_vertice_destino=None):
        self.set_grafica(grafica)
        self.set_id_vertice_inicial(id_vertice_inicial)
        self.set_id_vertice_destino(id_vertice_destino)
        self.set_id_vertice_actual(id_vertice_inicial)
        lista_vertices_no_visitados = []
        vertices_de_la_grafica = self.get_grafica().get_vertices()
        for vertice in vertices_de_la_grafica:
            lista_vertices_no_visitados.append(vertice.get_identificador())
        self.set_vertices_no_visitados(lista_vertices_no_visitados)
        self.set_vertices_visitados([])
        
        for vertice in vertices_de_la_grafica:
            id_de_este_vertice = vertice.get_identificador()
            if id_de_este_vertice == id_vertice_inicial:
                self.get_grafica().set_valorVertice(id_de_este_vertice, 0)
            else:
                self.get_grafica().set_valorVertice(id_de_este_vertice, math.inf)
        
    def set_grafica(self, grafica):
        self.__grafica = grafica
        
    def get_grafica(self):
        return self.__grafica
    
    def set_id_vertice_inicial(self, id_vertice_inicial):
        self.__id_vertice_inicial = id_vertice_inicial
        
    def get_id_vertice_inicial(self):
        return self.__id_vertice_inicial
    
    def set_id_vertice_destino(self, id_vertice_destino):
        self.__id_vertice_destino = id_vertice_destino
        
    def get_id_vertice_destino(self):
        return self.__id_vertice_destino   
    
    def set_id_vertice_actual(self, id_vertice_actual):
        self.__id_vertice_actual = id_vertice_actual
    
    def get_id_vertice_actual(self):
        return self.__id_vertice_actual
    
    def set_vertices_no_visitados(self, vertices_no_visitados):
        self.__vertices_no_visitados = vertices_no_visitados
        
    def get_vertices_no_visitados(self):
        return self.__vertices_no_visitados
    
    def set_vertices_visitados(self, vertices_visitados):
        self.__vertices_visitados = vertices_visitados
        
    def get_vertices_visitados(self):
        return self.__vertices_visitados
    
    def __str__(self):
        cadena = ""
        cadena += "Grafica: \n"+str(self.get_grafica())
        cadena += "VInicial: " +str(self.get_id_vertice_inicial())+"\n"
        if self.get_id_vertice_destino()!= None:    
            cadena += "VDestino: "+str(self.get_id_vertice_inicial())+"\n"
        cadena += "VActual: "+str(self.get_id_vertice_actual())+"\n"
        cadena += "VVisitados: "+str(self.get_vertices_visitados())+"\n"
        cadena += "VNoVisitados: "+str(self.get_vertices_no_visitados())+"\n"
        return cadena
        
    def __repr__(self):
        return self.__str__()

    def run_step(self):
        grafNodoActual_id = self.get_id_vertice_actual()
        vecinosNodoActual = self.get_grafica().get_vecinosDe(grafNodoActual_id)
        nodosNoVisitados = self.get_vertices_no_visitados()
        vecinosNoVisitado = []
        print (grafNodoActual_id)
        print (vecinosNodoActual)
        print (nodosNoVisitados)
        for id_vertice in vecinosNodoActual:
            if id_vertice in nodosNoVisitados:
                vecinosNoVisitado.append(id_vertice)
        print(vecinosNoVisitado)

        for id_vertice in vecinosNoVisitado:
            valorVerticeActual = self.get_grafica().get_valorVertice(grafNodoActual_id)
            pesoAristaQueConecta = self.get_grafica().get_pesoArista(grafNodoActual_id, id_vertice)
            pesoNuevo = valorVerticeActual+pesoAristaQueConecta
            pesoViejo = self.get_grafica().get_valorVertice(id_vertice)
            if pesoNuevo < pesoViejo:
                self.get_grafica().set_valorVertice(id_vertice, pesoNuevo)

        nodosVisitados = self.get_vertices_visitados()
        nodosVisitados.append(grafNodoActual_id)
        nodosNoVisitados.remove(grafNodoActual_id)
        self.set_vertices_no_visitados(nodosNoVisitados)
        self.set_vertices_visitados(nodosVisitados)
        
    def verifica_termino(self):
        nodosNoVisitados = self.get_vertices_no_visitados()
        nodosVisitados = self.get_vertices_visitados()
        grafNodoDestino_id = self.get_id_vertice_destino()
        if grafNodoDestino_id != None:
            if grafNodoDestino_id in nodosVisitados:
                return True
        
        algunoNoEsInfnito = False
        for id_vertice in nodosNoVisitados:
            pesoActualVertice = self.get_grafica().get_valorVertice(id_vertice)
            if pesoActualVertice != math.inf:
                algunoNoEsInfnito = True
        return not algunoNoEsInfnito
    
    def actualiza_nodo_actual(self):
        nodosNoVisitados = self.get_vertices_no_visitados()
        verticeMenorPeso = None
        menorPeso = None
        for id_vertice in nodosNoVisitados:
            if verticeMenorPeso == None:
                verticeMenorPeso = id_vertice
                menorPeso = self.get_grafica().get_valorVertice(id_vertice)
            
            pesoActualVertice = self.get_grafica().get_valorVertice(id_vertice)
            if pesoActualVertice < menorPeso:
                verticeMenorPeso = id_vertice
                menorPeso = pesoActualVertice
        self.set_id_vertice_actual(verticeMenorPeso)
        
    def recuperaCamino(self, vertice_objetivo):
        llegamosAlVerticeInicial = False
        verticesCamino = []
        while (not llegamosAlVerticeInicial):
            if vertice_objetivo == self.get_id_vertice_inicial():
                llegamosAlVerticeInicial = True
                break
            print (verticesCamino)
            pesoVertice = self.get_grafica().get_valorVertice(vertice_objetivo)
            vecinos = self.get_grafica().get_vecinosDe(vertice_objetivo)
            verticesCamino.append(vertice_objetivo)
            for vertice_id in vecinos:
                pesoVerticeVecino = self.get_grafica().get_valorVertice(vertice_id)
                pesoAristaConecta = self.get_grafica().get_pesoArista(vertice_objetivo, vertice_id)
                if pesoVertice == pesoVerticeVecino+pesoAristaConecta:
                    vertice_objetivo=vertice_id
                    break     
        verticesCamino.reverse()
        return verticesCamino
                
        
    
    def run(self):
        elalgoritmotermino = dijks.verifica_termino()
        print(self)
        while (elalgoritmotermino == False):
            self.run_step()
            elalgoritmotermino = dijks.verifica_termino()
            print(self)
            if (not elalgoritmotermino):
                self.actualiza_nodo_actual()
            

        
listaVertices = []
listaVertices.append(vertice(1))
listaVertices.append(vertice(2))
listaVertices.append(vertice(3))
listaVertices.append(vertice(4))
listaVertices.append(vertice(5))
listaVertices.append(vertice(6))
listaAristas = []
listaAristas.append(arista(1,2,3))
listaAristas.append(arista(2,3,2))
listaAristas.append(arista(1,3,4))
listaAristas.append(arista(3,4,2))
listaAristas.append(arista(3,5,7))
listaAristas.append(arista(4,6,5))
listaAristas.append(arista(5,6,1))
graf = grafica(listaVertices, listaAristas)
dijks = dijkstra(graf,1)
dijks.run()
print ("El camino entre el nodo inicial y el nodo 6 es:")
print (dijks.recuperaCamino(6))
#print(dijks)
#dijks.run_step()
#print(dijks)
#elalgortmotermino = dijks.verifica_termino()
#print(elalgortmotermino)



"""
grafNodoActual_id = dijks.get_id_vertice_actual()
vecinosNodoActual = dijks.get_grafica().get_vecinosDe(grafNodoActual_id)
nodosNoVisitados = dijks.get_vertices_no_visitados()
vecinosNoVisitado = []
print (grafNodoActual_id)
print (vecinosNodoActual)
print (nodosNoVisitados)
for id_vertice in vecinosNodoActual:
    if id_vertice in nodosNoVisitados:
        vecinosNoVisitado.append(id_vertice)
print(vecinosNoVisitado)

for id_vertice in vecinosNoVisitado:
    valorVerticeActual = dijks.get_grafica().get_valorVertice(grafNodoActual_id)
    pesoAristaQueConecta = dijks.get_grafica().get_pesoArista(grafNodoActual_id, id_vertice)
    pesoNuevo = valorVerticeActual+pesoAristaQueConecta
    pesoViejo = dijks.get_grafica().get_valorVertice(id_vertice)
    if pesoNuevo < pesoViejo:
        dijks.get_grafica().set_valorVertice(id_vertice, pesoNuevo)

nodosVisitados = dijks.get_vertices_visitados()
nodosVisitados.append(grafNodoActual_id)
nodosNoVisitados.remove(grafNodoActual_id)
dijks.set_vertices_no_visitados(nodosNoVisitados)
dijks.set_vertices_visitados(nodosVisitados)

nodosNoVisitados = dijks.get_vertices_no_visitados()
nodosVisitados = dijks.get_vertices_visitados()

grafNodoDestino_id = dijks.get_id_vertice_destino()
    
nuevoVerticeActual = None
pesoNuevoVerticeActual = None
for id_vertice in nodosNoVisitados:
    if nuevoVerticeActual == None:
        nuevoVerticeActual = id_vertice
        pesoNuevoVerticeActual = dijks.get_grafica().get_valorVertice(id_vertice)
        continue
    
    pesodeId_vertice = dijks.get_grafica().get_valorVertice(id_vertice)
    if pesodeId_vertice < pesoNuevoVerticeActual:
        nuevoVerticeActual = id_vertice
        pesoNuevoVerticeActual = pesodeId_vertice

dijks.set_id_vertice_actual(nuevoVerticeActual)
print(dijks)      
"""
        
        
        


"""
print(graf)
#print(graf.get_aristas())
#print(graf.get_vertices())
vecinosde1 = graf.get_vecinosDe(1)
pesoArista12 = graf.get_pesoArista(1, 2)
pesoArista21 = graf.get_pesoArista(2, 1)
print(vecinosde1)
print(pesoArista12)
print(pesoArista21)
valorVertice1 = graf.get_valorVertice(1)
valorVertice2 = graf.get_valorVertice(2)
print (valorVertice1)
print (valorVertice2)
costoLlegarAV2 = valorVertice1 + pesoArista12
print (str(costoLlegarAV2)+"<"+str(valorVertice2))
print (costoLlegarAV2 < valorVertice2)
graf.set_valorVertice(2,costoLlegarAV2)
nuevoValorVertice2 = graf.get_valorVertice(2)
print (nuevoValorVertice2)

#[2,3]



#Aristas
#verticeA
#verticeB
#Peso
"""      