# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 08:29:02 2020

@author: dario
"""

def merge(aoriginal, acopia, inicioizq, mitad, finalder):
    k = inicioizq
    i = inicioizq
    j = mitad + 1
    while i <= mitad and j <= finalder:
        if aoriginal[i] < aoriginal[j]:
            acopia[k] = aoriginal[i]
            i = i + 1
        else:
            acopia[k] = aoriginal[j]
            j = j + 1
        k = k + 1
    while i <= mitad:
        acopia[k] = aoriginal[i]
        i = i + 1
        k = k + 1
    while j <= finalder:
        acopia[k] = aoriginal[j]
        j = j + 1
        k = k + 1
    for i in range(inicioizq,finalder+1):
        aoriginal[i] = acopia[i]
        
arreglo = [5,8,10,12,4,7,9,11]
arreglocopia = arreglo.copy()

print(arreglo)
print(arreglocopia)
merge(arreglo,arreglocopia,0,3,7)
print(arreglo)
print(arreglocopia)

def mergesort(arreglo):
    inicio = 0
    final = len(arreglo) + 1
    acopia = arreglo.copy()
    m = 1
    while m <= final:
        for i in range(inicio, final, 2*m):
            inicioizq = i
            mitad = i + (m - 1)
            finalder = min(i+(2*m)-1, final)
            merge(arreglo, acopia, inicioizq, mitad, finalder)
        m = m*2
        
arr = [10,4,6,12,4,9,12,3,4,79]
print(arr)
mergesort(arr)
print(arr)
