# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 08:25:40 2021

@author: dario
"""

def InsertionRec(arr,n):
    if n <= 1:
        return    
    InsertionRec(arr,n-1)
    ultimo = arr[n-1]
    j = n-2
    while (j >= 0 and arr[j] > ultimo):
        arr[j+1] = arr[j]
        j = j-1
        arr[j+1]=ultimo
        
lista = [2,8,5,3,9,4]
n = len(lista)

InsertionRec(lista,n)

for i in range(n):
    print(lista[i], end="")