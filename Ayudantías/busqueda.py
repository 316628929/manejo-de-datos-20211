# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 08:53:46 2021

@author: dario
"""

def busquedaSecuencial(array,n):
    for indice in range(len(array)):
        if n == array[indice]:
            return(indice)
        
def busquedaBinaria(array,l,h,x):
    med = l//h
    indiceEncontrado = None
    if array[med] == x or l==h or l>h:
        if array[med] == x:
            return(indiceEncontrado)
        else:
            return(indiceEncontrado)
    else:
        if array[med] < x:
            indiceEncontrado = busquedaBinaria(array,med+1,h,x)
            return(indiceEncontrado)
        elif array[med] > x:
            indiceEncontrado = busquedaBinaria(array,l,med-1,x)
            return(indiceEncontrado)
        
        
array = [5,3,7,6,6,1,4]
indice = busquedaSecuencial(array, 6)
print(indice)