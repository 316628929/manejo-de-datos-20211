# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 08:15:17 2021

@author: dario
"""

def quicksortR(array,l,h):
    if l < h:
        posicionPivote = particiona(array, l, h)
        quicksortR(array,l,posicionPivote-1)
        quicksortR(array,posicionPivote+1,h)
        
def quicksortI(array):
    stack = []
    l = 0
    h = len(array)-1
    stack.append((l,h))
    while stack:
        l,h = stack.pop()
        posicionPivote = particiona(array,l,h)
        if l < posicionPivote-1:
            stack.append((l,posicionPivote-1))
        if h > posicionPivote+1:
            stack.append((posicionPivote+1,h))
       
def particiona(array,l,h):
    pivote = array[h]
    i = l
    j = h-1
    while i <= j:
        if array[i] > pivote and array[j] <= pivote:
            temp = array[j]
            array[i] = array[j]
            array[j] = temp
            i = i+1
            j = j-1
        if array[i] > pivote and array[j] > pivote:
            j = j-1
        if array[i] <= pivote and array[j] <= pivote:
            i = i+1
        if array[i] <= pivote and array[j] > pivote:
            i = i+1
            j = j-1
        temp = array[i]
        array[i] = pivote
        array[h] = temp
        return(i)
    
arr = [5,3,7,6,6,1,4]
l = 0
h = len(arr)-1
print(arr)
#particiona(arr,1,h)
quicksortR(arr, l, h)
print(arr)