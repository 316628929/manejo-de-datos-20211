# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 00:57:38 2021

@author: ASUS
"""

import simpy
"""
- Simpy es una biblioteca que ayuda a modelar eventos discretos
- Para modelar elementos que posean actividad haciendo uan descripción de los
procesos que efectuan
- Interactuan con un entorno, Envioroment()
- Efectuando eventos, env()
- Los procesos que realizan, función proceso o método de proceso 
(Depende de si es función o método de una clase)
Se podría decir, nota personal, que los eventos son los que hacen las grandes
pautas durante el proceso: los cambios bruscos.
- EVENTO IMPORTANTE: Timeout, evento que depende del tiempo, que se ejecuta
después de este tiempo determinado.

"""

#nos modela el proceso de un carro, se manejará y estacionará
#aleatoriamente, imprimiendo los tiempos en que realiza cada evento

def car(env):
    while True:
        print('Start parking at: %d' % env.now)
        parking_duration = 5
        #que duré estacionado 5 unidades de tiempo
        yield env.timeout(parking_duration)
        #yield se encarga de esperar a que le den la señal de ejecutar
        print('Start driving at: %d' % env.now)
        trip_duration = 2
        #manejé por 2 unidades de tiempo
        yield env.timeout(trip_duration)

env =  simpy.Environment()
env.process(car(env))
#generador de procesos
#Process, usarase como interacciones de proceso
"""Ejemplo común, de esperar que un proceso termine e interrumpir un
proceso mientras se espera un evento"""
env.run(until = 15)
#run, tiempo de finalización
"""run(until = 15), en esta instrucción le decimos que realice la
actividad de estacionarse y conducir por 15 unidades de tiempo
"""
