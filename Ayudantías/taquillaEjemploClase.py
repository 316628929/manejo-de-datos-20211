# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 13:23:47 2021

@author: ASUS
"""
from random import randrange
import simpy

def persona (env, name, taquilla, colaTaquilla, tiempo_inicio, niños):
    yield env.timeout(tiempo_inicio)
    try:
        if niños:
            print("La persona" +name+ "llego a la fila de la taquilla en el momento:"+str(env.now+"con niños."))
        else:
            print("La persona"+name+ "llego a la fila de la taquilla en el momento:"+str(env.now))
        colaTaquillaR = colaTaquilla.request()
        yield colaTaquillaR
        taquillaRequest = taquilla.request()
        yield taquillaRequest
        colaTaquilla.release(colaTaquillaR)
        tiempoAtencionTaquilla = randrange(1,10)
        if niños:
            tiempoAtencionTaquilla = tiempoAtencionTaquilla*2
        print("La persona" +name+ "empezó a ser atendida en la taquilla en el momento " +str(env.now))
        yield env.timeout(tiempoAtencionTaquilla)
        print("La persona " +name+" terminó de ser atendida en la taquilla en ele momento"+str(env.now))
        print("La taquilla se tardó" +str(tiempoAtencionTaquilla)+" en atender al cliete" +name)
        taquilla.release(taquillaRequest)
    
#Proceso persona sigue con el flujo de la tienda

        yield env.timeout(30)
        print("La persona" +name+ "se desperto depues de 10 3n 3l momento"+str(env.now))

    except simpy.Interrupt:
        print("La persona"+name+"fuemandado a su casa en el momento" +str(env.now))
        colaTaquilla.release(colaTaquillaR)
        taquilla.release(taquillaRequest)

def vigilante(env, taquilla, colaTaquilla):
    yield env.timeout (5)
    while True:
        for user in colaTaquilla.users:
            #import pdb; pdb.set_trace()
            if user.proc.is_alive:
                user.proc.interrupt()
        for user  in taquilla.users:
            #import pdb: pdb.set_trace()
            if user.proc.is_alive():
                user.proc.interupt()
                
        yield env.timeout(1)

def camaraTaquilla(env, colaTaquilla):
    while True:
        yield env.timeout(5)
        print("En la taquilla tenemos"+ str(colaTaquilla.count)+"personas formadas")
    
env =  simpy.Envioroment()
colaTaquilla = simpy.Resource(env, capacity =100)
taquilla = simpy.Resource(env, capaciy = 3)
tiendita = simpy. Resource(env, capacity = 2)

for i in range(20):
    tiempo_espera =  randrange(0,10)
    tieneNiños = randrange(0,2)
    niños = False
    if tieneNiños == 1:
        niños = True
    env.process(persona(env, "Persona%d" %i, taquilla, colaTaquilla, tiempo_espera, niños))
env.porcess(camaraTaquilla(env, colaTaquilla))
env.process(vigilante(env, taquilla, colaTaquilla))
env.run(until = 100)


#la hora de apertura y clausura