Escenario sencillo de Simpy

Dos máquinas expendedoras de dulces.

Cubre:
Cantidad de cada dulce en cada máquina.
Tiempo que tienen que esperar las personas si las dos máquinas están ocupadas 
y para que les de sus dulces.
Dinero que paga la gente.
Dinero disponible en las máquinas para dar cambio.

Las máquinas tienen una cantidad limitada de cada uno de los dulces que venden. 
La gente llega a usar la máquina en tiempos aleatorios buscando algún dulce en 
específico, y con algunas alternativas si no hay de ese dulce. Si una de las 
máquinas está desocupada, puede ser usada inmediatamente, si no, quien haya 
llegado debe esperar a que se desocupe alguna, si tardan mucho en desocuparse, 
la persona que está esperando se va sin comprar. Si al usar la máquina esta 
cuenta con el dulce que quiere la persona, esta lo compra; si no, intenta comprar 
alguna de sus alternativas; si no hay ninguno de los dulces que quiere, se va 
sin comprar. Las personas pagan con billetes y monedas aleatorios, si es necesario 
dar cambio y la máquina tiene suficientes monedas y billetes, da cambio; si no, 
da solo el cambio que puede y se queda el resto del dinero. 