Lunes
Se mencionó que se utilizaría una nueva herramienta que será de gran utilidad durante el semestre. 
Se comenzó haciendo uso de los puntos de ruptura, que provocan que el código corra hasta cierto 
punto. Se vio también el uso de #%%, que divide al código en secciones, permitiendo que se corran 
por separado estas celdas. Estas son herramientas para la depuración, que se utiliza para 
encontrar errores lógicos en el programa. Se utilizó la opción de ejecutar la línea seleccionada y 
se utilizó el explorador de variables en conjunto con las herramientas de depuración.

Martes
Se continuó checando la forma de depurar, comenzando con los puntos de ruptura. Se revisó la forma 
en la que se revisa cada una de las variables utilizadas, incluídas aquellas que vienen de clases 
y archivos distintos al que se está trabajando. Además de esto se siguió analizando la resolución 
del problema de transporte que se ha trabajado, esta vez con más detalle, pues el uso de las 
herramientas de depuración así lo permite.

Miércoles
Hubo ayudantía con Karla. Se nos presentó una alternativa a gitahead, que se utiliza para trabajar 
con git desde la consola, de la misma forma que en Linux. También se nos presentó la herramienta 
Overleaf, que sirve para trabajar con LaTeX dn línea, así como para editar documentos en equipo.

Jueves
Hubo ayudantía con Karla. Se vio la idea de Dijkstra, que es un algoritmo, que se usa para visitar 
los nodos de una gráfica, y encontrar la forma más eficiente de llegar de un nodo a otro. Se nos 
presentó la página URI, que nos puede servir para practicar y mejorar en Pyhton, e incluye retos 
de programación competitiva.

Viernes
No hubo clase real el viernes 30 de octubre de 2020, anno Domini.