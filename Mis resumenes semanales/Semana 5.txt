Lunes
Se comenzó a trabajar con el método de la esquina noroeste. Que es un algoritmo para la solución 
de problemas de transporte. En este se distribuyen las unidades que se quieren transportar entre 
los lugares a los que se quieren llevar usando una tabla, y escogiendo la opción que se encuentra 
en la esquina superior izquierda, la "esquina noroeste".

Martes
Se continuó trabajando con el problema de la esquina noroeste. Se trabajó en papel, para asegurar 
que este se entendiera de la mejor manera posible antes de empezar a trabajar con él en Python. 
Se determinó que este problema se puede resolver de manera iterativa. Se mostró al final un 
programa en el que se resuelve un problema de transporte usando el algoritmo de la esquina 
noroeste, en este se ocupó el módulo prettytable, que se utilizó para representar la tabla en la 
que se mostraron las ofertas y demandas.

Miércoles
Se continuó trabajando con el problema de la esquina noroeste. Esta vez se empezó a revisar el 
código de Pyrthon desde el inicio. Este se desarrolló utilizando listas de listas para guardar la 
información. Aunado a esto, se hizo uso tanto de módulos ajenos al programa mismo, como de clases 
ajenas al programa mismo. Se revisó también el uso de métodos para la resolución del problema. Se 
nos proporcionó un archivo que contiene las clases y los scripts necesarios para la resolución, 
estos se añadieron a nuestro repositorio en los directorios apropiados.

Jueves
Se trabajó para poder correr el archivo con la solución al problema de la esquina noroeste. Se 
agregó nuestro repositorio al Path de Spyder, y se añadió el módulo prettytable al entorno de 
Anaconda.

Viernes
Se trabajo en comprender el funcionamiento del programa de Python con el que se resolvió el 
problema de la esquina noroeste. Se vio que para este programa se hizo uso de clases, y se 
analizaron tanto los métodos como los atributos de las distintas clases que se ocupan, como las de 
las ciudades. Se vio que se utilizaron algunos atributos de manera privada, para trabajar con 
encapsulamiento y proteger a los elementos de nuestras clases. Se vio también como se utilizaron 
setters y getters. Se comenzó a ver también el problema de calcular los costos resultantes de la 
resolución de este problema.