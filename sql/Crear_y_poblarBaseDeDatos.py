import mysql.connector
from mysql.connector import errorcode

DB_NAME = 'oild_ManejoDeDatos'

TABLES = {}

TABLES['preguntas_respuestas'] = (
    "CREATE TABLE `preguntas_respuestas` ("
    "  `no_pregunta` int(2) NOT NULL AUTO_INCREMENT,"
    "  `pregunta` varchar(200) NOT NULL,"
    "  `respuesta` varchar(30) NOT NULL,"
    "  PRIMARY KEY (`no_pregunta`)"
    ") ENGINE=InnoDB")

TABLES['palabras_y_conceptos'] = (
    "CREATE TABLE `palabras_y_conceptos` ("
    "  `no_semana` int(2) NOT NULL AUTO_INCREMENT,"
    "  `palabras` varchar(300) NOT NULL,"
    "  PRIMARY KEY (`no_semana`)"
    ") ENGINE=InnoDB")

config = {
  'user': 'oild',
  'password': '316628929',
  'host': 'mysql-17763-0.cloudclusters.net',
  'port': '17792',
  'database': 'oild_ManejoDeDatos',
  'raise_on_warnings': True
}

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

try:
    cursor.execute("CREATE DATABASE IF NOT EXISTS " + DB_NAME)
except mysql.connector.Error as err:
    print("La base de datos " + DB_NAME + " ya existe")
    print(err.msg)
else:
    print("Se ha creado la base de datos " + DB_NAME )

cursor.execute("USE " + DB_NAME)

for table_name in TABLES:
    table_description = TABLES[table_name]
    try:
        print("Creando tabla {}: ".format(table_name), end='')
        cursor.execute(table_description)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("Ya existe.")
        else:
            print(err.msg)
    else:
        print("OK")

add_pregunta = ("INSERT INTO preguntas_respuestas"
               "(pregunta,respuesta)"
               "VALUES (%s, %s)")

add_semana = ("INSERT INTO palabras_y_conceptos"
               "(palabras)"
               "VALUES (%s)")

pregunta1 = ('¿Cómo se le llama al tipo de objeto en python que similar a una función ejecuta una serie de argumentos, pero que es solamente aplicable a objetos de una clase en específico?','Método')
pregunta2 = ('¿Cuál es el nombre de los métodos definidos con un doble guión bajo antes y después de su nombre?','Métodos dunder')
pregunta3 = ('¿Cómo se le conoce a los métodos usados para obtener el valor de algún atributo de un objeto de una clase?','Getters')
pregunta4 = ('¿Cómo de le conoce a los métodos usados para modificar el valor de algún atributo de un objeto de una clase?','Setters')
pregunta5 = ('¿A qué tipo de atributo no se puede acceder o modificar desde afuera de la clase a la que pertenece el objeto?','Atributo privado')
pregunta6 = ('¿Cómo se le conoce a un código en Python usado de forma indirecta en otro código?','Módulo')
pregunta7 = ('¿Cómo se le conoce a un código en Python que se usa de forma directa, como código principal?','Script')
pregunta8 = ('¿Qué herramienta de depuración permite correr el código hasta una línea determinada?','Punto de ruptura')
pregunta9 = ('¿Cómo se le llama al tipo de función que se puede definir de forma rápida y sencilla, en una sola línea, utilizando la palabra reservada "lambda"?','Función anónima')
pregunta10 = ('¿Qué módulo sirve para utilizar el debugger de Python desde el código mismo?','pdb')
pregunta11 = ('¿Qué herramienta del depurador permite ejecutar el código por bloques individuales?','Celdas')
pregunta12 = ('¿Qué tipo de algoritmo de ordenamiento divide listas a la mitad constantemente y luego las junta de forma ordenada?','Merge Sort')
pregunta13 = ('¿Qué forma de resolver un problema lo hace aplicando la solución al elemento anterior hasta llegar al inicial, llamado base?','Recursiva')
pregunta14 = ('¿Cuál es una forma más apropiada de referirse a los valores aleatorios generados por Python, considerando que se obtienen a partir de algoritmos definidos?','Pseudo-aleatorios')
pregunta15 = ('¿Qué biblioteca de Python es útil para trabajar con vectores?','Numpy')
pregunta16 = ('¿Cuál es el lenguaje de programación más comunmente utilizado para trabajar con bases de datos?','SQL')
pregunta17 = ('¿Qué tipo de aplicación permite a un usuario conectarse a un servidor?','Cliente')
pregunta18 = ('¿Qué tipo de llava en SQL permite identificar a cada elemento de una tabla?','Primario')
pregunta19 = ('¿Qué tipo de variable en SQL tiene un uso similar al de un entero, pero es de menor tamaño y ocupa menos espacio?','Smallint')
pregunta20 = ('¿Qué biblioteca de Python permite simular procesos en un ambiente determinado?','Simpy')

data_preguntas = [pregunta1,pregunta2,pregunta3,pregunta4,pregunta5,pregunta6,pregunta7,pregunta8,pregunta9,pregunta10,pregunta11,pregunta12,pregunta13,pregunta14,pregunta15,pregunta16,pregunta17,pregunta18,pregunta19,pregunta20]

for i in data_preguntas:
    cursor.execute(add_pregunta, i)
    no_pregunta = cursor.lastrowid
cnx.commit()

Semana1 = ('Python, VisualBasic, Excel, Matlab, SciLab, clase, objeto, atributos, solución, problemas, construcción, operaciones, binarias, métodos, dunder')
Semana2 = ('clase, métodos, dunder, init, indentación, problema, simplificar, función, implementación, setters, getters, valor, constructor, atributos, variable, privados, públicos, comando, pass, algoritmo')
Semana3 = ('método, solución, problemas, función, atributos, self, operaciones, objetos, setters, programa, flotantes, división, script, módulo, main, asignación, comparación, bool, call, variable')
Semana4 = ('código, biblioteca, módulo, importar, numpy, matplotlib, cadenas, listas, enteros, flotantes, condiciones, operadores, bool')
Semana5 = ('método, esquina noroeste, transporte, algoritmo, solución, problema, iterativa, PrettyTable, módulo, papel, clases, scripts, directorio, listas, path, atributos, privados, setters, getters, encapsulamiento')
Semana6 = ('puntos de ruptura, código, depuración, celdas, explorador, variables, problema, transporte, solución, clases, Djikstra, algoritmo')
Semana7 = ('algoritmo, Djikstra, clases, depuración, programa, objetos, predeterminado, implementación, variable, método, problema, transporte')
Semana8 = ('solución, problema, transporte, algoritmo, Vogel, Djikstra, código, implementar, ciclo, continue, método, función anónima, lambda, sorted, key, listas, clases, comentarios, función')
Semana9 = ('algoritmo, Djikstra, problema, función, solución, método, programa, papel, implementación, listas, ordenamiento, objeto, código, except, raise, comentario, depuración, argumento, sorted, diccionarios, celdas, try, transporte, punto de ruptura, esquina noroeste')
Semana10 = ('solución, método, problema, transporte, esquina noroeste, sorted, key, depuración, variables, global, implementación, explorador, papel, ordenar, enteros, burbuja, funciones, recursiva recursiva')
Semana11 = ('problema, implementación, método, listas, solución, índices, depuración, ciclo, ordenar, burbuja, BubbleSort, QuickSort, InsertionSort, CountingSort, HeapSort, algoritmo, MergeSort, lista, recursiva, algoritmo')
Semana12 = ('solución, butbuja, recursiva, método, implementación, MergeSort, listas, algoritmo, iterativa, caso base, problema, ordenamiento')
Semana13 = ('clases, scripts, recursiva, algoritmo, InsertionSort, programa, iterativa, ordenamiento, implementación, problema, valores, aleatorios, variable, random, pseudoaleatorio, módulo, ciclo, función, seed, lista, intervalo')
Semana14 = ('valores, aleatorios, predeterminados, funcion, listas, comprensión, Gauss, vectores, numpy, biblioteca, random, listas, estructuras, almacenamiento, algoritmo, QuickSort, implementación, entero, matriz, clase, función, comentario, documentar')
Semana15 = ('valores, aleatorios, random, seed, función, lista, stack, recursión, memoria, almacenamiento, objeto, clase, base, datos, lenguaje, SQL, MySQL, búsqueda, secuencial, ordenamiento, shell, servidor, cliente, tabla')
Semana16 = ('bases, datos, manejador, SQL, MySQL, tabla, puertos, servidor, almacenamiento, variable, llave, campo, tipo de dato, null, extra, default, Simpy, simular, procesos, ambiente, shell, enum, smallint, valor')

data_semana = [Semana1,Semana2,Semana3,Semana4,Semana5,Semana6,Semana7,Semana8,Semana9,Semana10,Semana11,Semana12,Semana13,Semana14,Semana15,Semana16]

for i in data_semana:
    cursor.execute(add_semana, (i,))
    no_semana = cursor.lastrowid
cnx.commit()

cursor.close()
cnx.close()