# Clase de complejos

#primer intento complejos
# con base en su forma binomial
class complejo():
    def __init__(self, real, imaginario):#constructor que nos dice de que partes esta formado un complejo
        #self.r = real
        #self.i = imaginario
        self.setR(real)
        self.setI(imaginario)
        
    def __str__(self):#para que le de formato a los números complejos
        #return "{} + {}i".format(self.r, self.i)
        return "{} + {}i".format(self.getR(), self.getI())
    "MÓDULO"
    def modulo(self):
        #return (self.r**2 + self.i**2)**(1/2)
        return(self.getR()**2 + self.getI()**2)**(1/2)

    "CONJUGADO"
    def conjugado(self):
        #return complejo(self.r, -self.i)
        return complejo(self.getR(), -self.getI())
    
    "PRODUCTO"
    def producto(self,c1):
        #r = (self.r * c1.r) - ( self.i * c1.i)
        #i = (self.r * c1.i) + (self.i * c1.r)
        r = (self.getR() * c1.getR()) - ( self.getI() * c1.getI())
        i = (self.getR() * c1.getI()) + (self.getI() * c1.getR())
        return complejo (r, i)
    
    "SUMA"
    def suma(self,c1):
        #r = self.r + c1.r
        #i = self.i + c1.i
        r = self.getR() + c1.getR()
        i = self.getI() + c1.getI()
        return complejo(r, i)
        
    "RESTA"
    def resta(self, c1):
        #r = self.r - c1.r
        #i = self.i - c1.i
        r = self.getR() - c1.getR()
        i = self.getI() - c1.getI()
        return complejo(r,i)
    
    "COCIENTE"
    def cociente(self, c1):
        #r = ((self.r * c1.r) + (self.i * c1.i ))/(c1.r ** 2) + (c1.i **2)
        #i = ((self.i * c1.r) + (self.r * c1.i))/(self.i**2 + c1.i**2)
        r = ((self.getR() * c1.getR()) + (self.getI() * c1.getI() ))/(c1.getR() ** 2) + (c1.getI() **2)
        i = ((self.getI() * c1.getR()) + (self.getR() * c1.getI()))/(self.getI()**2 + c1.getI()**2)
        return complejo(r,i)
    "setters"
    #real
    def setR(self, real):
        self.__r = real
    #imaginario
    def setI(self, imaginario):
        self.__i  = imaginario
    
    "getters"
    #real
    def getR(self):
        return self.__r
    #imaginario
    def getI(self):
        return self.__i

a = complejo(1,2)
b = complejo(2,1)
c = complejo(3,-4)
#checar método modulo
h = a.modulo()
#conjugado
d = a.conjugado()
#producto
e =  a.producto(b)
#suma
f = b.suma(c)
#cociente
g =  b.cociente(c)
#resta
i = a.resta(c)
print(a)
print(b)
print(c)
print("El modulo de ", a, " es: ",h)
print("El conjugado de ", a,"es: ", d)
print("El producto de los complejos",a," y ", b," es: ",e)
print("La suma de los complejos", b, "y ", c,"es:", f)
print("El cociente de los complejos", b, " y ", c, " es: ", g)
print("La resta de complejos ", a, " y ", c, "es: ",i )