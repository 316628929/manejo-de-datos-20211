# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 18:06:12 2021

@author: dario
"""

# Se resuelven juegos de forma estrategica con estrategias puras de dos 
# jugadores, sin limitacion al numero de estrategias.
# Se obtienen los equilibrios de Nash, los perfiles eficientes segun Pareto,
# una visualización de la matriz de pagos, y los conjuntos de mejores
# respuestas de cada jugador.


#%%
# Esta es la clase de jugadores.
# Los jugadores cuentan con un nombre y un conjunto de estrategias.
class jugador():
    def __init__(self, name, conjunto_estrategias):
        self.set_nombre(name)
        self.set_estrategias(conjunto_estrategias)
        
    def set_nombre(self, name):
        self.__nombre = name
        
    def get_nombre(self):
        return(self.__nombre)
    
    def set_estrategias(self,conjunto_estrategias):
        self.__estrategias = conjunto_estrategias
        
    def get_estrategias(self):
        return(self.__estrategias)
    
#%%
# Esta es la clase del juego.
# Se define con un nombre, el conjunto de jugdores y la funcion de pagos.
class juego():
    def __init__(self,name:str,conjunto_jugadores:list,funcion_pagos):
        self.set_nombre(name)
        self.set_jugadores(conjunto_jugadores)
        self.set_funcion_pagos(funcion_pagos)
        self.estrategias()
    
    # Se convierten las estrategias de los jugadores a indices para trabajarlos
    # con facilidad
    def estrategias(self):
        j1 = self.__conjunto_jugadores[0]
        j2 = self.__conjunto_jugadores[1]
        estrategias1 = j1.get_estrategias()
        estrategias2 = j2.get_estrategias()
        indices1 = []
        indices2 = []
        c = 0
        d = 0
        for _ in estrategias1:
            indices1.append(c)
            c += 1
        for _ in estrategias2:
            indices2.append(d)
            d += 1
        self.__conjunto_indices = (indices1,indices2)
        
    def set_nombre(self, name:str):
        self.__nombre = name
        
    def get_nombre(self):
        return(self.__nombre)
        
    def set_jugadores(self,conjunto_jugadores:list):
        self.__conjunto_jugadores = conjunto_jugadores
        
    def set_funcion_pagos(self,funcion_pagos):
        self.__funcion_pagos = funcion_pagos
        
    def get_jugadores(self):
        return(self.__conjunto_jugadores)
        #return(self.__conjunto_jugadores[0].get_nombre(),self.__conjunto_jugadores[1].get_nombre())
    
    def get_funcion_pagos(self):
        return(self.__funcion_pagos)
    
    # Aqui­ se hace el producto cartesiano de las estrategias de ambos
    # jugadores para obtener una lista con todos los perfiles de estrategia.
    # Esto es un simple producto cartesiano de los conjuntos de estrategias de 
    # cada uno de los jugadores.    
    def perfiles(self):
        estrategias1 = self.__conjunto_indices[0]
        estrategias2 = self.__conjunto_indices[1]
        perfiles = []
        for i in estrategias1:
            for j in estrategias2:
                perfiles.append((i,j))
        return(perfiles)
    
    # Usando los perfiles de pago, se evaluan en la funcion de pagos y se 
    # genera la matriz de pagos asociada al juego.
    # Esto se hace con una lista de listas, en las que el primer nivel de 
    # representa las filas de la matriz.
    # Las celdas de la matriz contienen los perfiles de pago ya obtenidos 
    # evaluados en la función de pagos.
    def matriz_pagos(self):
        matriz = []
        for i in self.__conjunto_indices[0]:
            matriz.append([])
            for j in self.__conjunto_indices[1]:
                matriz[i].append(self.__funcion_pagos(i,j))
        return(matriz)
    
    # Se obtiene el conjunto de mejores respuestas para alguno de los jugadores.
    # El algoritmo es distinto para el primer jugador que para el segundo, 
    # pues eso facilitó su implementación.
    # Para esto se compararoon los pagos de los perfiles con el resto de los
    # que se encuentran en la misma fila o columna, y si el perfil evaluado 
    # tiene mejores o iguales pagos que las alternativas para el respectivo 
    # jugador, este se agraga a la lista de mejores respuestas.
    def mejor_respuesta(self,jugador:jugador):
        """
        Muestra los índices de las mejores estrategias de un jugador específico.

        Parameters
        ----------
        jugador : jugador
            DESCRIPTION.

        Returns
        -------
        None.

        """
        if jugador == self.__conjunto_jugadores[0]:
            matriz = self.matriz_pagos()
            l = []
            for j in self.__conjunto_indices[1]:
                m = matriz[1][j][0]
                for i in self.__conjunto_indices[0]:
                    if matriz[i][j][0] > m:
                        m = matriz[i][j][0]
                for i in self.__conjunto_indices[0]:
                    if matriz[i][j][0] == m:
                        l.append([i,j])
        else:
            matriz = self.matriz_pagos()
            l = []
            for i in self.__conjunto_indices[0]:
                m = matriz[i][0][1]
                for j in self.__conjunto_indices[1]:
                    if matriz[i][j][1]> m:
                        m = matriz[i][j][1]
                for j in self.__conjunto_indices[1]:
                    if matriz[i][j][1] == m:
                        l.append([i,j])
        return(l)
        
    # Se obtiene el conjunto de equilibrios de Nash del juego.
    # Estos se obtienen haciendo una simple comparación entre los conjuntos de 
    # mejores respuestas de ambos jugadores. Si un perfil está presente en 
    # ambos conjuntos, este es un equilibrio de Nash con estrategias puras.
    def equilibrios_nash(self):
        """
        Muestra el conjunto de equilibrios de Nash con estrategias puras.

        Returns
        -------
        None.

        """
        BR1 = self.mejor_respuesta(self.__conjunto_jugadores[0])
        BR2 = self.mejor_respuesta(self.__conjunto_jugadores[1])
        equilibrios = []
        for i in BR1:
            if i in BR2:
                #equilibrios.append(i)
                equilibrios.append([self.__conjunto_jugadores[0].get_estrategias()[i[0]],self.__conjunto_jugadores[1].get_estrategias()[i[1]]])
        return(equilibrios)
    
    # Obtiene el conjunto de mejores respuestas con nombres de estrategias, 
    # no con indices de la lista de estrategias.
    def mejores_respuestas(self,jugador:jugador):
        """
        Muestra el conjunto de mejores estrategias de un jugador particular.

        Parameters
        ----------
        jugador : jugador
            DESCRIPTION.

        Returns
        -------
        None.

        """
        BR = []
        for i in self.mejor_respuesta(jugador):
            BR.append([self.__conjunto_jugadores[0].get_estrategias()[i[0]],self.__conjunto_jugadores[1].get_estrategias()[i[1]]])
        return(BR)
            
    # Se muestra la matriz de pagos.
    # Se usa PrettyTable para mostrar la matriz como una tabla.
    def mostrar_matriz_pagos(self):
        """
        Muestra la matriz de pagos asociada al juego.

        Returns
        -------
        None.

        """
        j1 = self.__conjunto_jugadores[0]
        j2 = self.__conjunto_jugadores[1]
        estrategias1 = j1.get_estrategias()
        estrategias2 = j2.get_estrategias()
        from prettytable import PrettyTable
        matriz = self.matriz_pagos()        
        t = PrettyTable()
        t.field_names = [""] + [i for i in estrategias2]
        c = 0
        for i in matriz:
            renglon = [estrategias1[c]] + [j for j in i]
            t.add_row(renglon)
            c +=1
        print(t)
    
    # Se obtiene el conjunto perfiles eficientes segun Pareto.
    # Esto se hace comparando cada uno de los perfiles ya evaluados, y sus 
    # imágenes bajo la función de pagos.
    # Si para un perfil existe otro cuya imagen tiene al menos una coordenada 
    # con mejor pago, y la otra no menor, el perfil evaluado puede ser 
    # mejorado, y no es Pareto eficiente. SI este no es el caso, no se pueden 
    # mejorar los pagos sin afectar al menos a un jugador, y el perfil se 
    # agrega a la lista de eficientes segun Pareto.
    def pareto_eficiente(self):
        """
        Obtiene los perfiles eficientes según Pareto.

        Returns
        -------
        None.

        """
        lista_paretos = []
        perfiles = self.perfiles()
        for i in perfiles:
            a = 0
            for j in perfiles:
                if self.__funcion_pagos(j[0],j[1])[0] > self.__funcion_pagos(i[0],i[1])[0]:
                    if self.__funcion_pagos(j[0],j[1])[1] >= self.__funcion_pagos(i[0],i[1])[1]:
                        a = 1
                elif self.__funcion_pagos(j[0],j[1])[1] > self.__funcion_pagos(i[0],i[1])[1]:
                    if self.__funcion_pagos(j[0],j[1])[0] >= self.__funcion_pagos(i[0],i[1])[0]:
                        a = 1
            if a == 0:
                #lista_paretos.append(i)
                lista_paretos.append([self.__conjunto_jugadores[0].get_estrategias()[i[0]],self.__conjunto_jugadores[1].get_estrategias()[i[1]]])
        return(lista_paretos)
    
#%%

