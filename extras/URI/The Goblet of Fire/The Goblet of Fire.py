from functools import reduce
from math import sqrt

def remd(lista):
	lista1 = lista.copy()
	for i in lista:
		lista1.remove(i)
		if i in lista1:
			lista.remove(i)
	return(lista)

def factors(n):
        step = 2 if n%2 else 1
        return set(reduce(list.__add__,
                    ([i, n//i] for i in range(1, int(sqrt(n))+1, step) if n % i == 0)))

def factor(numberToFactor, arr=list()):
        i = 2
        maximum = numberToFactor / 2 + 1
        while i < maximum:
                if numberToFactor % i == 0:
                        return factor(numberToFactor/i,arr + [i])
                i += 1
        return list(set(arr + [numberToFactor]))   

def pdl2(n,p):
        l = factors(n)
        lp = factor(n)
        lpp = lp.copy()
        for i in lpp:
                if i < p:
                        lp.remove(i)
        ll = l.copy()
        for i in ll:
                for j in lp:
                        if i%j == 0:
                                l.remove(i)
                                break
        return(len(l))
                
def pdl(n,p):
        lis = []
        for i in d:
                if i < p:
                        lis.append(i)
        su = 0
        for i in lis:
                su += len(d[i])
        return(su)


num = int(input())
pm = factor(num)[-1]

d = {}
d[1] = [1]

for i in factor(num):
        d[i] = [i]

for i in factors(num):
        r = factor(i)[-1]
        if i not in d[r]:
                d[r].append(i)

while True:
    try:
        line = int(input())

        num = num * line

        if line > pm:
                prim = []
                for i in d:
                        for j in d[i]:
                                prim.append(j*line)
                d[line] = prim
        elif line == pm:
                lasta = []
                for i in d:
                        for k in d[i]:
                                lasta.append(line*k)
                for i in lasta:
                        d[pm].append(i)
        elif line < pm:
                for i in d:
                        nuev = []
                        if line > i:
                                for k in d[i]:
                                        nuev.append(k*line)
                                for j in nuev:
                                        d[line].append(j)
                        elif line == i:
                                for k in d[i]:
                                        nuev.append(k*line)
                                for j in nuev:
                                        d[i].append(j)
                        elif line < i:
                                for k in d[i]:
                                        nuev.append(k*line)
                                for j in nuev:
                                        d[i].append(j)
        for i in d:
                d[i]=remd(d[i])
                
                        
                        
        print(pdl(num,line))

        pm = max(pm,line)

    except EOFError:
        break;
