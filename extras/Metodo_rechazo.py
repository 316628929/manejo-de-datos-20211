# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 17:04:38 2021

@author: dario
"""

from math import sin
from random import gauss, random

import matplotlib.pyplot as plt


def campana(n,b=100,mu=0,sd=2):
    l = []
    for _ in range(n):
        l.append(gauss(mu,sd))
    plt.hist(l,b)
    plt.show()
    return(l)

def curva(f,n,b=10,a=-1,c=1):
    # f es la función, n es la cantidad de puntos a evaluar,
    # b es la cantidad de secciones en el histogrma, 
    # a y c son los extremos del intervalo a evaluar.
    l = []
    li = []
    lis = []
    for _ in range(n):
        l.append([a+random()*(c-a),a+random()*(c-a)])
    for i in l:
        if i[1] <= f(i[0]):
            li.append(i[1])
            lis.append(i[0])
    plt.style.use('fivethirtyeight')
    plt.scatter(lis,li)
    plt.show()
    plt.hist(lis,bins=b)
    plt.show()
    return(li)

# Algunos ejemplos
curva(lambda x: sin(x), 10000, b=300, a=-5, c=5)
curva(lambda x: x**3, 10000, b=300, a=-10, c=10)
curva(lambda x: 4-x**2, 10000, b=300, a=-10, c=10)
curva(lambda x: 8*x+x**2-4*x**3, 10000, b=300, a=-20, c=20)
curva(lambda x: sin(1/x), 30000, b=300, a=-1, c=1)
