from math import sqrt
from random import random, gauss
import matplotlib.pyplot as plt

def dardos(n):
    '''
    n es el número de dardos a lanzar
    '''
    l=[]
    lx=[]
    ly=[]
    lx1=[]
    ly1=[]
    for i in range(n):
            x = [-1+random()*2,-1+random()*2]
            lx1.append(x[0])
            ly1.append(x[1])
            if sqrt(x[1]**2 + x[0]**2)<=1:
                    l.append(x)
                    lx.append(x[0])
                    ly.append(x[1])
    plt.scatter(lx1,ly1)
    plt.scatter(lx,ly)
    return(4*len(l)/n)

def integral(f,n,a,b):
    '''
    f es la función a integrar,
    se debe definir antes como función en Python;
    n es el número de valores a evaluar;
    [a,b) es el intervalo a evaluar
    '''
    #l = []
    li = []
    for i in range(n):
        #l.append(a+(random()*(b-a)))
    #for i in l:
        li.append(f(a+(random()*(b-a))))
        #li.append(f(i))
    return((b-a)*(sum(li)/len(li)))

def campana(n):
    l = []
    for i in range(n):
        l.append(gauss(1,1))
    plt.hist(l,10)
    return(l)

def curva(f,n,b=10,a=-1,c=1):
    # f es la función, n es la cantidad de puntos a evaluar,
    # b es la cantidad de secciones en el histogrma, 
    # a y c son los extremos del intervalos a evaluar.
    l = []
    li = []
    lis = []
    for i in range(n):
        l.append([a+random()*(c-a),a+random()*(c-a)])
    for i in l:
        if i[1] <= f(i[0]):
            li.append(i[1])
            lis.append(i[0])
    plt.style.use('fivethirtyeight')
    plt.scatter(lis,li)
    plt.show()
    plt.hist(lis,bins=b)
    plt.show()
    
#Se corren algunos ejemplos

# from math import sin

# print("Aproximación de pi con 100 tiros: "+str(dardos(100))+"\n")

# print("Aproximación de pi con 1000 tiros: "+str(dardos(1000))+"\n")

# print("Aproximación de la integral de la raiz cuadrada de x en el intervalo [0,2), con 100 valores: "+str(integral(sqrt,100,0,2))+"\n")

# print("Aproximación de la integral del seno de x en el intervalo [-5,5), con 200 valores: "+str(integral(sin,200,-5,5))+"\n")

# def func(x):
#     return(0.1*(x+2)*(x-2)*(x-5) + 2)
# print("Aproximación de la integral del polinomio 0.1(x+2)(x-2)(x-5)+2 en el intervalo [-2,6), con 1000 valores: "+str(integral(func,1000,-2,6))+"\n")

# def func2(x):
#     return(x**2 + x + 14)
# print("Aproximación de la integral del polinomio (x^2 + x + 14) en el intervalo [3,16), con 1000 valores: "+str(integral(func2,1000,3,16))+"\n")
