# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 13:54:32 2021

@author: dario
"""
#import sys
#sys.path.append('C:\\Users\\dario\\Documents\\manejo-de-datos-20211')

from clases.Juegos.JuegoClases import jugador, juego

j1 = jugador("nombre",[i for i in range(0,7)])
j2 = jugador("nombre2",[i for i in range(0,7)])

def pagos(a,b):
	J1 = 0
	J2 = 0
	if a == b:
		J1 = 0.5
		J2 = 0.5
	elif a < b:
		J1 = 1
		J2 = -1
	elif a > b:
		J1 = -1
		J2 = 1
	return([J1,J2])

juego1 = juego("Juego",[j1,j2],pagos)

print(f"El conjunto de mejores respuestas del jugador 1 es {juego1.mejores_respuestas(j1)}")
print(f"El conjunto de mejores respuestas del jugador 2 es {juego1.mejores_respuestas(j2)}")
print(f"El conjunto de equilibrios de Nash con estrategias puras es {juego1.equilibrios_nash()}")
print(f"El conjunto de perfiles Pareto eficientes es: {juego1.pareto_eficiente()}")
juego1.mostrar_matriz_pagos()
 
print("\n"+"#"*100+"\n"+"#"*100+"\n")

#%%
 
def funcion(x,y):
    if x < y:
        return([1,4])
    elif x > y:
        return([0,-2])
    elif x == 0:
        return([2,4])
    else:
        return([1,-1])
        
jugador2 = jugador("nombre",["x","y"])
jugador1 = jugador("nombre",["x","y"])
juego2 = juego("Juego",[jugador1,jugador2],funcion)

print(f"El conjunto de mejores respuestas del jugador 1 es {juego2.mejores_respuestas(jugador1)}")
print(f"El conjunto de mejores respuestas del jugador 2 es {juego2.mejores_respuestas(jugador2)}")
print(f"El conjunto de equilibrios de Nash con estrategias puras es {juego2.equilibrios_nash()}")
print(f"El conjunto de perfiles Pareto eficientes es: {juego2.pareto_eficiente()}")
juego2.mostrar_matriz_pagos()


print("\n"+"#"*100+"\n"+"#"*100+"\n")
#%%

def funcion2(x,y):
    if x == 0:
        if y == 0:
            return([1,0])
        elif y == 1:
            return([0,1])
        elif y == 2:
            return([2,-1])
    elif x == 1:
        if y == 0:
            return([0,0])
        elif y == 1:
            return([2,4])
        elif y == 2:
            return([4,3])
    elif x == 2:
        if y == 0:
            return([2,1])
        elif y == 1:
            return([1,0])
        elif y == 2:
            return([3,2])
        
J1 = jugador("UNO",["A","B","C"])
J2 = jugador("DOS",["X","Y","Z"])
juego3 = juego("Juego",[J1,J2],funcion2)

print(f"El conjunto de mejores respuestas del jugador 1 es {juego3.mejores_respuestas(J1)}")
print(f"El conjunto de mejores respuestas del jugador 2 es {juego3.mejores_respuestas(J2)}")
print(f"El conjunto de equilibrios de Nash con estrategias puras es {juego3.equilibrios_nash()}")
print(f"El conjunto de perfiles Pareto eficientes es: {juego3.pareto_eficiente()}")
juego3.mostrar_matriz_pagos()   

print("\n"+"#"*100+"\n"+"#"*100+"\n")

#%%

def funcion3(x,y):
    if x == 0:
        return([2,3])
    elif x == 1:
        if y == 0:
            return([2,1])
        elif y == 1:
            return([1,2])
    elif x == 2:
        if y == 0:
            return([0,1])
        elif y == 1:
            return([2,0])
        
JUGADOR1 = jugador("ONE",["a","b","c"])
JUGADOR2 = jugador("TWO",["A","B"])
juego4 = juego("Juego4",[JUGADOR1,JUGADOR2],funcion3)

print(f"El conjunto de mejores respuestas del jugador 1 es {juego4.mejores_respuestas(JUGADOR1)}")
print(f"El conjunto de mejores respuestas del jugador 2 es {juego4.mejores_respuestas(JUGADOR2)}")
print(f"El conjunto de equilibrios de Nash con estrategias puras es {juego4.equilibrios_nash()}")
print(f"El conjunto de perfiles Pareto eficientes es: {juego4.pareto_eficiente()}")
juego4.mostrar_matriz_pagos() 

print("\n"+"#"*100+"\n"+"#"*100+"\n")

#%%

def JuegoAleatorio():
    from random import randint
    EstrategiasAleatorias1 = [i for i in range(0,randint(2,7))]
    EstrategiasAleatorias2 = [i for i in range(0,randint(2,7))]
    JugadorAleatorio1 = jugador("UNO",EstrategiasAleatorias1)
    JugadorAleatorio2 = jugador("DOS",EstrategiasAleatorias2)
    ListaPerfiles = []
    for i in JugadorAleatorio1.get_estrategias():
        for j in JugadorAleatorio2.get_estrategias():
            ListaPerfiles.append([i,j])
    ListaPagos = [[randint(-5,10),randint(-5,10)] for i in ListaPerfiles]
    def FuncionPagos(x,y):        
        for i in ListaPerfiles:
            if x == i[0]:
                if y == i [1]:
                    return(ListaPagos[ListaPerfiles.index(i)])
    JuegoAleatorio = juego("Aleatorio",[JugadorAleatorio1,JugadorAleatorio2],FuncionPagos)
    return(JuegoAleatorio)

game = JuegoAleatorio()
print(f"El conjunto de mejores respuestas del jugador 1 es {game.mejores_respuestas(game.get_jugadores()[0])}")
print(f"El conjunto de mejores respuestas del jugador 2 es {game.mejores_respuestas(game.get_jugadores()[1])}")
print(f"El conjunto de equilibrios de Nash con estrategias puras es {game.equilibrios_nash()}")
print(f"El conjunto de perfiles Pareto eficientes es: {game.pareto_eficiente()}")
game.mostrar_matriz_pagos() 