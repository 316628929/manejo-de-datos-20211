#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 20:38:58 2020

@author: luis
"""
def mostrar(frutas):
    print("-"*20)
    for fruta in frutas:
        print(*fruta)
    
frutas = [ ["pera",["verde", 15]],
          ["manzana",["roja",20]],
          ["platano",["verde",13]],
          ["tuna",["amarilla",21]],
          ["mora",["azul",39]]]

mostrar(frutas)
#%%
frutasOrdenadas=sorted(frutas)
mostrar(frutasOrdenadas)

#%%
frutasOrdenadas=sorted(frutas,key=lambda v:v[1])
mostrar(frutasOrdenadas)
#%%
frutasOrdenadas=sorted(frutas,key=lambda v:v[1][1])
mostrar(frutasOrdenadas)
#%%
def mostrarD(frutas):
    print("-"*20)
    for x in frutas:
        print(x,frutas[x])

frutasd = { "pera":["verde", 15],
          "manzana":["roja",20],
          "platano":["verde",13],
          "tuna":["amarilla",21],
          "mora":["azul",39]}
mostrarD(frutasd)
#%%
#frutasOrdenadasD=sorted(frutasd)
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[0])}
mostrarD(frutasOrdenadasD)
#%%
#frutasOrdenadas=sorted(frutasd,key=lambda v:v[0])
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[1])}
mostrarD(frutasOrdenadasD)
#%%
#frutasOrdenadas=sorted(frutas,key=lambda v:v[1][1])
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[1][1])}
mostrarD(frutasOrdenadasD)