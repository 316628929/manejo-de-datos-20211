# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 08:44:44 2021

@author: dario
"""

from random import random, seed, choice, sample, shuffle
valor = random()
print(valor)

#seed(1)
valor = random()
print(valor)

for _ in range(10):
    valor = random()
    print(valor)
    
def valor_aleatorio(a,b):
    valor = int(a+random()*(b-a))
    return(valor)

print(valor_aleatorio(7,13))

#for i in range(valor_aleatorio(10,15)):
#    print(100+random()*50)
    
def n_valores_aleatorios(a,b,N=10):
    n = valor_aleatorio(1,N)
    valores = []
    for i in range(n):
        valores.append(valor_aleatorio(a, b))
    return(valores)

def n_valores_aleatoriosLC(a,b,m=1,N=10):
    n = valor_aleatorio(m,N)
    valores = [valor_aleatorio(a,b) for i in range(n)]
    return(valores)

# Se generan valores enteros aleatorios
from random import randint
for _ in range(10):
    value = randint(3,10)
    print(value)

# Se generan valores aleatorios con ditribución gaussiana.    
from random import gauss
for _ in range(10):
    value = gauss(3,10)
    print(value)

#%%
# Se utiliza el módulo numpy para generar valores aleatorios.
from numpy.random import randn
valores = randn(10)
print(valores)

#%%
# Se usa la función choice y se crea una lista por compensión.
seed(1)
secuencia = [i for i in range(20)]
print(secuencia)
for _ in range(5):
    seleccion = choice(secuencia)
    print(seleccion)
    
#%%

lista = [choice(secuencia) for i in range(7)]

#%%
secuencia = [i for i in range(20)]
subsecuencia = sample(secuencia,5)
print(subsecuencia)

#%%
